package com.example.main;

import com.example.common.domain.User;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class UserBean implements Serializable {

    private User user;

    @PostConstruct
    private void init () {
        user = new User();
        user.setId(1L);
        user.setName("User");
    }

    public User getUser() {
        return user;
    }
}
